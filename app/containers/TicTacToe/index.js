import { memo } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { TicTacToe } from './TicTacToe';
import {
  actionSelectSquare,
  actionStepBack,
  actionStepForward,
} from './actions';
import {
  selectSquares,
  selectFirstPlayerWins,
  selectSecondPlayerWins,
  selectAmountOfSets,
} from './selectors';

const mapStateToProps = createStructuredSelector({
  squares: selectSquares(),
  firstPlayerWins: selectFirstPlayerWins(),
  secondPlayerWins: selectSecondPlayerWins(),
  amountOfSets: selectAmountOfSets(),
});

const mapDispatchToProps = dispatch => ({
  handleStepBack: () => dispatch(actionStepBack()),
  handleStepForward: () => dispatch(actionStepForward()),
  handleSelectSquare: payload => dispatch(actionSelectSquare(payload)),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(TicTacToe);
