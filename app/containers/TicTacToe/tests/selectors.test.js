import {
  selectTicTacToeDomain,
  selectSquares,
  selectFirstPlayerWins,
  selectSecondPlayerWins,
  selectAmountOfSets,
} from '../selectors';

const initialState = {
  ticTacToe: {
    isFirstPlayerNext: true,
    firstPlayerWins: 0,
    secondPlayerWins: 0,
    amountOfSets: 0,
    historyGame: [
      {
        squares: Array(9).fill(null),
      },
    ],
    currentStep: 0,
  },
};

beforeAll(() => {
  selectTicTacToeDomain(initialState);
});

describe('selectTicTacToeDomain', () => {
  it('Expect to have domain state', () => {
    expect(selectTicTacToeDomain(initialState)).toEqual(initialState.ticTacToe);
  });

  it('Expect to have select Squares', () => {
    expect(selectSquares()(initialState)).toEqual(
      initialState.ticTacToe.historyGame[initialState.ticTacToe.currentStep]
        .squares,
    );
  });

  it('Expect to have select First Player Wins', () => {
    expect(selectFirstPlayerWins()(initialState)).toEqual(
      initialState.ticTacToe.firstPlayerWins,
    );
  });

  it('Expect to have select Second Player Wins', () => {
    expect(selectSecondPlayerWins()(initialState)).toEqual(
      initialState.ticTacToe.secondPlayerWins,
    );
  });

  it('Expect to have select Amount Of Sets', () => {
    expect(selectAmountOfSets()(initialState)).toEqual(
      initialState.ticTacToe.amountOfSets,
    );
  });
});
