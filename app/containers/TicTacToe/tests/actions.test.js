import {
  actionSelectSquare,
  actionStepBack,
  actionStepForward,
} from '../actions';
import { SELECT_SQUARE, STEP_BACK, STEP_FORWARD } from '../constants';

describe('TicTacToe actions', () => {
  it('has a type of SELECT_SQUARE', () => {
    const expected = {
      type: SELECT_SQUARE,
      payload: {
        squareIndex: 0,
      },
    };

    expect(actionSelectSquare({ squareIndex: 0 })).toEqual(expected);
  });

  it('has a type of STEP_BACK', () => {
    const expected = { type: STEP_BACK };

    expect(actionStepBack()).toEqual(expected);
  });

  it('has a type of STEP_FORWARD', () => {
    const expected = { type: STEP_FORWARD };

    expect(actionStepForward()).toEqual(expected);
  });
});
