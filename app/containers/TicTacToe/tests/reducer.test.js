import { ticTacToeReducer } from '../reducer';
import {
  actionSelectSquare,
  actionStepBack,
  actionStepForward,
} from '../actions';

describe('ticTacToeReducer', () => {
  const state = {
    isFirstPlayerNext: true,
    firstPlayerWins: 0,
    secondPlayerWins: 0,
    amountOfSets: 0,
    historyGame: [
      {
        squares: Array(9).fill(null),
      },
    ],
    currentStep: 0,
  };

  it('returns the initial state', () => {
    const expectedResult = state;
    expect(ticTacToeReducer(undefined, {})).toEqual(expectedResult);
  });

  it('returns the add history step', () => {
    const expectedResult = {
      ...state,
      currentStep: 2,
      isFirstPlayerNext: true,
      historyGame: [
        {
          squares: Array(9).fill(null),
        },
        {
          squares: ['X', null, null, null, null, null, null, null, null],
        },
        {
          squares: ['X', 'O', null, null, null, null, null, null, null],
        },
      ],
    };
    const newState = ticTacToeReducer(
      state,
      actionSelectSquare({ squareIndex: 0 }),
    );

    expect(
      ticTacToeReducer(newState, actionSelectSquare({ squareIndex: 1 })),
    ).toEqual(expectedResult);
  });

  it('no add history step', () => {
    const expectedResult = {
      ...state,
      currentStep: 1,
      isFirstPlayerNext: false,
      historyGame: [
        {
          squares: Array(9).fill(null),
        },
        {
          squares: ['X', null, null, null, null, null, null, null, null],
        },
      ],
    };
    const state1 = ticTacToeReducer(
      state,
      actionSelectSquare({ squareIndex: 0 }),
    );

    expect(
      ticTacToeReducer(state1, actionSelectSquare({ squareIndex: 0 })),
    ).toEqual(expectedResult);
  });

  it('return first Player Wins state', () => {
    const expectedResult = {
      ...state,
      amountOfSets: 1,
      firstPlayerWins: 1,
      historyGame: [
        {
          squares: [null, null, null, null, null, null, null, null, null],
        },
      ],
    };
    const state1 = ticTacToeReducer(
      state,
      actionSelectSquare({ squareIndex: 0 }),
    );
    const state2 = ticTacToeReducer(
      state1,
      actionSelectSquare({ squareIndex: 3 }),
    );
    const state3 = ticTacToeReducer(
      state2,
      actionSelectSquare({ squareIndex: 1 }),
    );
    const state4 = ticTacToeReducer(
      state3,
      actionSelectSquare({ squareIndex: 4 }),
    );

    expect(
      ticTacToeReducer(state4, actionSelectSquare({ squareIndex: 2 })),
    ).toEqual(expectedResult);
  });

  it('return second Player Wins state', () => {
    const expectedResult = {
      ...state,
      amountOfSets: 1,
      secondPlayerWins: 1,
      historyGame: [
        {
          squares: [null, null, null, null, null, null, null, null, null],
        },
      ],
    };
    const state1 = ticTacToeReducer(
      state,
      actionSelectSquare({ squareIndex: 3 }),
    );
    const state2 = ticTacToeReducer(
      state1,
      actionSelectSquare({ squareIndex: 0 }),
    );
    const state3 = ticTacToeReducer(
      state2,
      actionSelectSquare({ squareIndex: 4 }),
    );
    const state4 = ticTacToeReducer(
      state3,
      actionSelectSquare({ squareIndex: 1 }),
    );
    const state5 = ticTacToeReducer(
      state4,
      actionSelectSquare({ squareIndex: 6 }),
    );

    expect(
      ticTacToeReducer(state5, actionSelectSquare({ squareIndex: 2 })),
    ).toEqual(expectedResult);
  });

  it('STEP_BACK ==> return replace history state', () => {
    const expectedResult = {
      ...state,
      currentStep: 2,
      isFirstPlayerNext: true,
      historyGame: [
        {
          squares: [null, null, null, null, null, null, null, null, null],
        },
        {
          squares: ['X', null, null, null, null, null, null, null, null],
        },
        {
          squares: ['X', null, null, null, null, null, null, null, 'O'],
        },
      ],
    };
    const state1 = ticTacToeReducer(
      state,
      actionSelectSquare({ squareIndex: 0 }),
    );
    const state2 = ticTacToeReducer(
      state1,
      actionSelectSquare({ squareIndex: 1 }),
    );
    const state3 = ticTacToeReducer(state2, actionStepBack());

    expect(
      ticTacToeReducer(state3, actionSelectSquare({ squareIndex: 8 })),
    ).toEqual(expectedResult);
  });

  it('STEP_BACK ==> return default state', () => {
    expect(ticTacToeReducer(state, actionStepBack())).toEqual(state);
  });

  it('STEP_FORWARD ==> step > history.length ==> return state', () => {
    expect(ticTacToeReducer(state, actionStepForward())).toEqual(state);
  });

  it('STEP_FORWARD ==> return update state', () => {
    const expectedResult = {
      ...state,
      currentStep: 4,
      isFirstPlayerNext: true,
      historyGame: [
        {
          squares: [null, null, null, null, null, null, null, null, null],
        },
        {
          squares: ['X', null, null, null, null, null, null, null, null],
        },
        {
          squares: ['X', 'O', null, null, null, null, null, null, null],
        },
        {
          squares: ['X', 'O', 'X', null, null, null, null, null, null],
        },
        {
          squares: ['X', 'O', 'X', null, null, null, null, null, 'O'],
        },
      ],
    };
    const state1 = ticTacToeReducer(
      state,
      actionSelectSquare({ squareIndex: 0 }),
    );
    const state2 = ticTacToeReducer(
      state1,
      actionSelectSquare({ squareIndex: 1 }),
    );
    const state3 = ticTacToeReducer(
      state2,
      actionSelectSquare({ squareIndex: 2 }),
    );
    const state4 = ticTacToeReducer(
      state3,
      actionSelectSquare({ squareIndex: 3 }),
    );

    const state5 = ticTacToeReducer(state4, actionStepBack());
    const state6 = ticTacToeReducer(state5, actionStepBack());
    const state7 = ticTacToeReducer(state6, actionStepForward());

    expect(
      ticTacToeReducer(state7, actionSelectSquare({ squareIndex: 8 })),
    ).toEqual(expectedResult);
  });
});
