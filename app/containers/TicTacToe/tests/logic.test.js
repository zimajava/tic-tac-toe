/* eslint-disable no-restricted-syntax */
import { calculateWinner } from '../logic';

const winnerCombinationX = [
  ['X', 'X', 'X', null, null, null, null, null, null],
  [null, null, null, 'X', 'X', 'X', null, null, null],
  [null, null, null, null, null, null, 'X', 'X', 'X'],
  ['X', null, null, 'X', null, null, 'X', null, null],
  [null, 'X', null, null, 'X', null, null, 'X', null],
  [null, null, 'X', null, null, 'X', null, null, 'X'],
  ['X', null, null, null, 'X', null, null, null, 'X'],
  [null, null, 'X', null, 'X', null, 'X', null, null],
];

const winnerCombinationO = [
  ['O', 'O', 'O', null, null, null, null, null, null],
  [null, null, null, 'O', 'O', 'O', null, null, null],
  [null, null, null, null, null, null, 'O', 'O', 'O'],
  ['O', null, null, 'O', null, null, 'O', null, null],
  [null, 'O', null, null, 'O', null, null, 'O', null],
  [null, null, 'O', null, null, 'O', null, null, 'O'],
  ['O', null, null, null, 'O', null, null, null, 'O'],
  [null, null, 'O', null, 'O', null, 'O', null, null],
];

const drawCombination = [
  ['O', 'X', 'X', 'X', 'O', 'O', 'O', 'X', 'X'],
  ['X', 'X', 'O', 'O', 'O', 'X', 'X', 'X', 'O'],
  ['X', 'O', 'X', 'X', 'O', 'O', 'O', 'X', 'X'],
  ['X', 'X', 'O', 'O', 'O', 'X', 'X', 'O', 'X'],
  ['X', 'O', 'O', 'O', 'X', 'X', 'X', 'X', 'O'],
  ['O', 'O', 'X', 'X', 'X', 'O', 'O', 'X', 'X'],
  ['X', 'O', 'X', 'O', 'X', 'O', 'O', 'X', 'O'],
  ['O', 'X', 'O', 'O', 'X', 'O', 'X', 'O', 'X'],
];

describe('calculateWinner', () => {
  describe('Expect winner "X"', () => {
    winnerCombinationX.forEach(combination => {
      it(`Expect to have winner "X" with args: ${combination}`, () => {
        expect(calculateWinner(combination)).toEqual('X');
      });
    });
  });

  describe('Expect winner "O"', () => {
    winnerCombinationO.forEach(combination => {
      it(`Expect to have winner "O" with args: ${combination}`, () => {
        expect(calculateWinner(combination)).toEqual('O');
      });
    });
  });

  describe('Expect "draw"', () => {
    drawCombination.forEach(combination => {
      it(`Expect to have "draw" with args: ${combination}`, () => {
        expect(calculateWinner(combination)).toEqual('draw');
      });
    });
  });
});
