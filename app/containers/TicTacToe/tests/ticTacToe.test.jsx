import React from 'react';
import { shallow } from 'enzyme';

import { TicTacToe } from '../TicTacToe';

const props = {
  squares: [],
  amountOfSets: 0,
  firstPlayerWins: 0,
  secondPlayerWins: 0,
  handleStepBack: () => null,
  handleStepForward: () => null,
  handleSelectSquare: () => null,
};

describe('<TicTacToe />', () => {
  it('Should render and match the snapshot', () => {
    const component = shallow(<TicTacToe {...props} />);
    expect(component).toMatchSnapshot();
  });
});
