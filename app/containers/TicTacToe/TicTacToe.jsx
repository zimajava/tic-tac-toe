import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import { useInjectReducer } from 'utils/injectReducer';
import { ticTacToeReducer } from './reducer';
import { Board, Button } from './components';
import { messages } from './messages';
import './styles.scss';

const propTypes = {
  squares: PropTypes.array.isRequired,
  firstPlayerWins: PropTypes.number.isRequired,
  secondPlayerWins: PropTypes.number.isRequired,
  amountOfSets: PropTypes.number.isRequired,
  handleSelectSquare: PropTypes.func.isRequired,
  handleStepBack: PropTypes.func.isRequired,
  handleStepForward: PropTypes.func.isRequired,
};

export const TicTacToe = props => {
  const {
    squares,
    firstPlayerWins,
    secondPlayerWins,
    amountOfSets,
    handleSelectSquare,
    handleStepBack,
    handleStepForward,
  } = props;

  useInjectReducer({ key: 'ticTacToe', reducer: ticTacToeReducer });

  return (
    <div className="game">
      <div className="game__history history">
        <Button className="history__back back" handleCallback={handleStepBack}>
          <span className="back__arrow">{'\u2B60'}</span>
          <FormattedMessage {...messages.historyBack} />
        </Button>
        <Button
          className="history__forward forward"
          handleCallback={handleStepForward}
        >
          <FormattedMessage {...messages.historyForward} />
          <span className="forward__arrow">{'\u279D'}</span>
        </Button>
      </div>
      <Board squares={squares} handleCallback={handleSelectSquare} />
      <div>
        <FormattedMessage {...messages.setsPlayed} />: {amountOfSets}
      </div>
      <div>
        <FormattedMessage {...messages.player1Wins} />: {firstPlayerWins}
      </div>
      <div>
        <FormattedMessage {...messages.player2Wins} />: {secondPlayerWins}
      </div>
    </div>
  );
};

TicTacToe.propTypes = propTypes;
