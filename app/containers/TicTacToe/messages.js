import { defineMessages } from 'react-intl';

export const scope = 'app.containers.TicTacToe';

export const messages = defineMessages({
  historyBack: {
    id: `${scope}.historyBack`,
    defaultMessage: 'Step Back',
  },
  historyForward: {
    id: `${scope}.historyForward`,
    defaultMessage: 'Step Forward',
  },
  setsPlayed: {
    id: `${scope}.setsPlayed`,
    defaultMessage: 'Sets played',
  },
  player1Wins: {
    id: `${scope}.player1Wins`,
    defaultMessage: 'Player 1 wins',
  },
  player2Wins: {
    id: `${scope}.player2Wins`,
    defaultMessage: 'Player 2 wins',
  },
});
