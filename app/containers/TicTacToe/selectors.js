import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectTicTacToeDomain = state => state.ticTacToe || initialState;
const selectFirstPlayerWins = () =>
  createSelector(
    selectTicTacToeDomain,
    substate => substate.firstPlayerWins,
  );
const selectSecondPlayerWins = () =>
  createSelector(
    selectTicTacToeDomain,
    substate => substate.secondPlayerWins,
  );
const selectAmountOfSets = () =>
  createSelector(
    selectTicTacToeDomain,
    substate => substate.amountOfSets,
  );
const selectSquares = () =>
  createSelector(
    selectTicTacToeDomain,
    substate => substate.historyGame[substate.currentStep].squares,
  );

export {
  selectTicTacToeDomain,
  selectSquares,
  selectFirstPlayerWins,
  selectSecondPlayerWins,
  selectAmountOfSets,
};
