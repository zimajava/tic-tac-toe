import { SELECT_SQUARE, STEP_BACK, STEP_FORWARD } from './constants';

export const actionSelectSquare = payload => ({ type: SELECT_SQUARE, payload });
export const actionStepBack = () => ({ type: STEP_BACK });
export const actionStepForward = () => ({ type: STEP_FORWARD });
