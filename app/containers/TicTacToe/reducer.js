import { SELECT_SQUARE, STEP_BACK, STEP_FORWARD } from './constants';
import { calculateWinner } from './logic';

export const initialState = {
  isFirstPlayerNext: true,
  firstPlayerWins: 0,
  secondPlayerWins: 0,
  amountOfSets: 0,
  historyGame: [
    {
      squares: Array(9).fill(null),
    },
  ],
  currentStep: 0,
};

export const ticTacToeReducer = (state = initialState, action) => {
  const {
    isFirstPlayerNext,
    historyGame,
    currentStep,
    firstPlayerWins,
    secondPlayerWins,
    amountOfSets,
  } = state;
  const { type, payload } = action;

  let history = historyGame.slice();
  const current = history[currentStep];
  const squares = current.squares.slice();

  switch (type) {
    case SELECT_SQUARE: {
      const { squareIndex } = payload;

      if (squares[squareIndex]) {
        return state;
      }

      squares[squareIndex] = isFirstPlayerNext ? 'X' : 'O';
      const winner = calculateWinner(squares);

      if (winner) {
        return {
          isFirstPlayerNext: initialState.isFirstPlayerNext,
          firstPlayerWins:
            winner === 'X' ? firstPlayerWins + 1 : firstPlayerWins,
          secondPlayerWins:
            winner === 'O' ? secondPlayerWins + 1 : secondPlayerWins,
          amountOfSets: amountOfSets + 1,
          historyGame: initialState.historyGame,
          currentStep: initialState.currentStep,
        };
      }

      if (history.length - 1 > currentStep) {
        history = historyGame.slice(0, currentStep + 1);
      }

      return {
        isFirstPlayerNext: !isFirstPlayerNext,
        firstPlayerWins,
        secondPlayerWins,
        amountOfSets,
        historyGame: history.concat([
          {
            squares,
          },
        ]),
        currentStep: currentStep + 1,
      };
    }
    case STEP_BACK: {
      const step = currentStep - 1;

      if (step < 0) {
        return state;
      }

      return {
        ...state,
        currentStep: step,
        isFirstPlayerNext: !isFirstPlayerNext,
      };
    }
    case STEP_FORWARD: {
      const step = currentStep + 1;

      if (step > history.length - 1) {
        return state;
      }

      return {
        ...state,
        currentStep: step,
        isFirstPlayerNext: !isFirstPlayerNext,
      };
    }
    default:
      return state;
  }
};
