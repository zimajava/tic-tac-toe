import React from 'react';
import { shallow } from 'enzyme';

import { Button } from '../Button';

const setup = () => {
  const props = {
    classNames: 'row__cell',
    payload: { squareIndex: 0 },
    children: '0',
    handleCallback: jest.fn(),
  };

  const output = shallow(<Button {...props} />);

  return {
    props,
    output,
  };
};

describe('<Button />', () => {
  it('should render and match the snapshot', () => {
    const { output } = setup();

    expect(output).toMatchSnapshot();
  });

  describe('handleCallback', () => {
    const {
      output,
      props: { handleCallback, payload, classNames },
    } = setup();
    const event = { key: ' ', preventDefault: () => null };
    afterEach(() => {
      handleCallback.mockClear();
    });

    it(`Click ==> Expect to be called with index: ${payload}`, () => {
      output.find(`div.${classNames}`).simulate('click');

      expect(handleCallback).toBeCalledWith(payload);
    });

    it(`onKeyDown " " ==> Expect to be called with index: ${payload}`, () => {
      output.find(`div.${classNames}`).simulate('keydown', event);

      expect(handleCallback).toBeCalledWith(payload);
    });

    it(`onKeyDown "Enter" ==> Expect to be called with index: ${payload}`, () => {
      event.key = 'Enter';
      output.find(`div.${classNames}`).simulate('keydown', event);

      expect(handleCallback).toBeCalledWith(payload);
    });

    it(`onKeyDown "Spacebar" ==> Expect to be called with index: ${payload}`, () => {
      event.key = 'Spacebar';
      output.find(`div.${classNames}`).simulate('keydown', event);

      expect(handleCallback).toBeCalledWith(payload);
    });

    it(`onKeyDown ==> Expect not be called`, () => {
      event.key = 'Tab';
      output.find(`div.${classNames}`).simulate('keydown', event);

      expect(handleCallback).not.toBeCalled();
    });
  });
});
