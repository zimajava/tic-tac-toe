import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  classNames: PropTypes.string,
  handleCallback: PropTypes.func.isRequired,
  payload: PropTypes.object,
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
};

export const Button = props => {
  const { classNames, handleCallback, payload, children } = props;

  const handleBtnKeyDown = (e, callback, data) => {
    if (e.key === ' ' || e.key === 'Enter' || e.key === 'Spacebar') {
      e.preventDefault();
      callback(data);
    }
  };

  return (
    <div
      className={classNames}
      tabIndex="0"
      role="button"
      onClick={() => handleCallback(payload)}
      onKeyDown={e => handleBtnKeyDown(e, handleCallback, payload)}
    >
      {children}
    </div>
  );
};

Button.propTypes = propTypes;
