import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '../Button';

const propTypes = {
  squares: PropTypes.array,
  handleCallback: PropTypes.func.isRequired,
};

export const Board = props => {
  const { squares, handleCallback } = props;

  const getSquare = squareRow =>
    squareRow.map(squareIndex => (
      <Button
        key={squareIndex}
        classNames="row__cell"
        payload={{ squareIndex }}
        handleCallback={handleCallback}
      >
        {squares[squareIndex]}
      </Button>
    ));

  return (
    <div className="game__board board">
      <div className="board__row row">{getSquare([0, 1, 2])}</div>
      <div className="board__row row">{getSquare([3, 4, 5])}</div>
      <div className="board__row row">{getSquare([6, 7, 8])}</div>
    </div>
  );
};

Board.propTypes = propTypes;
