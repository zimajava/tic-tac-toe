import React from 'react';
import { shallow } from 'enzyme';

import { Board } from '../Board';

const setup = () => {
  const props = {
    squares: [],
    handleCallback: jest.fn(),
  };

  const output = shallow(<Board {...props} />);

  return {
    props,
    output,
  };
};

describe('<Board />', () => {
  it('should render and match the snapshot', () => {
    const { output } = setup();

    expect(output).toMatchSnapshot();
  });
});
