import { defineMessages } from 'react-intl';

export const scope = 'app.containers.NotFoundPage';

export const messages = defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the NotFoundPage container!',
  },
});
