import React from 'react';
import { Switch, Route } from 'react-router-dom';

import TicTacToe from 'containers/TicTacToe/Loadable';
import NotFoundPage from 'components/NotFoundPage/Loadable';
import GlobalStyle from '../global-styles';

export const AppRouter = () => (
  <>
    <Switch>
      <Route exact path="/" component={TicTacToe} />
      <Route component={NotFoundPage} />
    </Switch>
    <GlobalStyle />
  </>
);
