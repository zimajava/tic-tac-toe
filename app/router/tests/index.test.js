import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';

import { AppRouter } from '../AppRouter';

const renderer = new ShallowRenderer();

describe('<App />', () => {
  it('should render and match the snapshot', () => {
    renderer.render(<AppRouter />);
    const renderedOutput = renderer.getRenderOutput();
    expect(renderedOutput).toMatchSnapshot();
  });
});
